const CDP = require('chrome-remote-interface');
const {wait} = require('./util');
const baseSettingsCaptureScreenShot = {format: 'jpeg', quality: 70};

//TODO fix duplication

/**
 * Take full page screenshot
 *
 * @param url
 * @return {Promise<Buffer>}
 */
async function takeFullScreenshot(url) {
    const client = await CDP();

    try {
        const {Page} = client;

        await Page.enable();
        await Page.navigate({url: url});
        await Page.loadEventFired();
        await wait();
        const {data} = await Page.captureScreenshot(baseSettingsCaptureScreenShot);

        return new Buffer(data, 'base64')
    } catch (e) {
        throw e;
    } finally {
        await client.close();
    }
}

async function takeTweetScreenshot(url) {
    const chrome = await CDP();
    try {
        const {Page, DOM} = chrome;

        await Promise.all([Page.enable(), DOM.enable()]);
        await Page.navigate({url: url});
        await Page.loadEventFired();
        await wait();

        const {root} = await DOM.getDocument();

        let tweetModalRef = await DOM.querySelector({
            nodeId: root.nodeId,
            selector: '.PermalinkOverlay-modal'
        });
        let tweetModalBoxModel = await DOM.getBoxModel(tweetModalRef);
        let [x, y] = tweetModalBoxModel.model.content;

        let tweetRef = await DOM.querySelector({
            nodeId: tweetModalRef.nodeId,
            selector: '.permalink-tweet-container'
        });

        let tweetBoxModel = await DOM.getBoxModel(tweetRef);
        let {width, height} = tweetBoxModel.model;

        const {data} = await Page.captureScreenshot(baseSettingsCaptureScreenShot);
        const buffer = new Buffer(data, 'base64');

        let heightOffset = 58;

        let retweetRef = await DOM.querySelector({
            nodeId: tweetModalRef.nodeId,
            selector: '.tweet-stats-container'
        });

        let retweetBoxModel = await DOM.getBoxModel(retweetRef);
        let {height:retweetHeight} = retweetBoxModel.model;
        heightOffset += retweetHeight;


        return cropImage(buffer, x + 5, y + 5, width - 5, height - heightOffset);
    } catch (e) {
        throw e;
    } finally {
        await chrome.close();
    }
}

async function takeTikTokScreenShot(url) {
    const chrome = await CDP();
    try {
        const {Page, DOM} = chrome;

        await Promise.all([Page.enable(), DOM.enable()]);
        await Page.navigate({url: url});
        await Page.loadEventFired();
        await wait();

        const {root} = await DOM.getDocument();

        let postContainerRef = await DOM.querySelector({
            nodeId: root.nodeId,
            selector: '.video-feed-item'
        });

        let postContainerBoxModel = await DOM.getBoxModel(postContainerRef);



        const {data} = await Page.captureScreenshot(baseSettingsCaptureScreenShot);
        const buffer = new Buffer(data, 'base64');
        const [borderX1, borderY1] = postContainerBoxModel.model.content;


        console.log(borderX1, borderY1, width, height);

        return cropImage(buffer, borderX1-15, borderY1-10, 505, 730);

    } catch (e) {
        throw e;
    } finally {
        await chrome.close();
    }
}

async function takeInstagramPostScreenShot(url) {
    const chrome = await CDP();
    try {
        const {Page, DOM} = chrome;

        await Promise.all([Page.enable(), DOM.enable()]);
        await Page.navigate({url: url});
        await Page.loadEventFired();
        await wait();

        const {root} = await DOM.getDocument();

        let postContainerRef = await DOM.querySelector({
            nodeId: root.nodeId,
            selector: 'sdfasdf'
        });

        let postContainerBoxModel = await DOM.getBoxModel(postContainerRef);

        console.log(postContainerBoxModel)

        const {data} = await Page.captureScreenshot(baseSettingsCaptureScreenShot);
        const buffer = new Buffer(data, 'base64');
        const [borderX1, borderY1] = postContainerBoxModel.model.content;
        const {width, height} = postContainerBoxModel.model;


        return cropImage(buffer, borderX1, borderY1, width, height);

    } catch (e) {
        throw e;
    } finally {
        await chrome.close();
    }
}

async function takeYoutubeScreenShot(url) {
    const chrome = await CDP();

    try {
        const {Page, DOM} = chrome;

        await Promise.all([Page.enable(), DOM.enable()]);
        Page.setAdBlockingEnabled = true;
        await Page.navigate({url: url});
        await Page.loadEventFired();
        await wait();

        const {data} = await Page.captureScreenshot(baseSettingsCaptureScreenShot);
        const buffer = new Buffer(data, 'base64');

        const {root} = await DOM.getDocument();

        let videoContainerRef = await DOM.querySelector({
            nodeId: root.nodeId,
            selector: '#player-container'
        });

        let videoContainerBoxModel = await DOM.getBoxModel(videoContainerRef);
        console.log(videoContainerBoxModel);
        const [borderX1, borderY1] = videoContainerBoxModel.model.content;
        const {width, height} = videoContainerBoxModel.model;

        let infoContainerRef = await DOM.querySelector({
            nodeId: root.nodeId,
            selector: '#info'
        });
        let infoBoxModel = await DOM.getBoxModel(infoContainerRef);

        const {height: infoHeight} = infoBoxModel.model;
        return cropImage(buffer, borderX1 - 5, borderY1, width + 10, height+infoHeight);
    } catch (e) {
        throw e;
    } finally {
        await chrome.close();
    }
}

/**
 * Crop buffer
 *
 * @param {Buffer} buffer
 * @param {int} x
 * @param {int} y
 * @param {int} width
 * @param {int} height
 * @return {Promise}
 */
async function cropImage(buffer, x, y, width, height) {
    const jimp = require('jimp');
    return new Promise((resolve, reject) => {
        jimp
            .read(buffer)
            .then(img => img.crop(x, y, width, height))
            .then(img => {
                img.getBuffer('image/jpeg', (err, newBuffer) => {
                    err ? reject(err) : resolve(newBuffer)
                })
            });
    })
}

module.exports = {
    takeFullScreenshot,
    takeTweetScreenshot,
    takeInstagramPostScreenShot,
    takeYoutubeScreenShot,
    takeTikTokScreenShot
};
