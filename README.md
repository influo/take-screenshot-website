Lambda function to take screenshot of website

**`INITIAL`**
- make sure you have nodejs 7.6+
- npm install

**`BUILD FOR LAMBDA`**
- npm run lambdaBuild
- upload build zip to s3 server + save location
- Deploy then on lambda trough s3 download.

**`LOCAL TEST`**
- change event.json if desired
- npm run build
- npm run localTest