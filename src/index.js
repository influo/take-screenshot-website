require('babel-polyfill');
const fs = require('fs');
const launchChrome = require('@serverless-chrome/lambda')
const {
	takeFullScreenshot,
	takeTweetScreenshot,
	takeInstagramPostScreenShot,
	takeYoutubeScreenShot,
	takeTikTokScreenShot
} = require('./takeScreenShot');
const {saveImage} = require('./util');

exports.handler = async function (event, context) {
	let conf = {
		flags: ['--window-size=1920x1080', '--hide-scrollbars', '--headless']
	};

	const launcher = await launchChrome(conf);
	const {url, action} = event;
	const {name: bucketName, path: folder, key: fileKey} = event.bucket;

	console.log('event', event);
	try {
		switch (action) {
			case 'TW': {
				const imageBuffer = await takeTweetScreenshot(url);
				const ref = await saveImage(imageBuffer, bucketName, `${folder}/${fileKey}`);
				context.succeed(ref);
				break;
			}

			case 'IG': {
				const imageBuffer = await takeInstagramPostScreenShot(url)
				const ref = await saveImage(imageBuffer, bucketName, `${folder}/${fileKey}`);
				context.succeed(ref);
				break;
			}


			case 'YT': {
				const imageBuffer = await takeYoutubeScreenShot(url);
				const ref = await saveImage(imageBuffer, bucketName, `${folder}/${fileKey}`);
				context.succeed(ref);
				break;
			}

			case 'TT':
				const imageBuffer = await takeTikTokScreenShot(url);
				const ref = await saveImage(imageBuffer, bucketName, `${folder}/${fileKey}`);
				context.succeed(ref);
				break;

			case 'FB':
			default: {
				const imageBuffer = await takeFullScreenshot(url);
				const ref = await saveImage(imageBuffer, bucketName, `${folder}/${fileKey}`);
				context.succeed(ref);
				break;
			}
		}
	} catch (e) {
		context.fail(e)
	}
};