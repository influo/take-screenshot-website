
const fs = require('fs');

/**
 *
 * @param {int=} timeInMs
 * @return {Promise}
 */
function wait(timeInMs) {
  return new Promise((resolve) => {
    setTimeout(_ => resolve('done waiting'), timeInMs || 2000)
  })
}

/**
 *
 * @param buffer
 * @param fullKey
 */
function saveImageLocal(buffer, fullKey) {
  console.log(fullKey);
  return new Promise((resolve, reject) => {
    fs.writeFile(fullKey, buffer, function(err) {
      if(err) {
        reject(err);
        return;
      }

      resolve('success');
    });
  })
}

/**
 * Save image based on env
 * @param buffer
 * @param bucketName
 * @param fullKey
 */
function saveImage(buffer, bucketName, fullKey) {
  if(process.env.NODE_ENV) {
    return saveImageLocal(buffer, fullKey);
  }

  return saveImageToS3(buffer, bucketName, fullKey)
}

/**
 * save image to s3
 * @param {Buffer} buffer
 * @param bucketName
 * @param fullKey
 *
 * @return {Promise<>}
 */
function saveImageToS3(buffer, bucketName, fullKey) {
  console.log(fullKey);
  const AWS = require("aws-sdk");
  const s3 = new AWS.S3();

  return new Promise(function(resolve, reject) {
    s3.putObject({
      "Bucket": bucketName,
      "Key": fullKey,
      "Body": buffer,
      'ACL': 'public-read',
      "ContentType": "image/jpeg"
    }, function (err, data) {
      buffer = null;
      if(err) {
        reject(err);
        return;
      }
      resolve(data);
    });
  })
}
module.exports = {
  wait,
  saveImage
};